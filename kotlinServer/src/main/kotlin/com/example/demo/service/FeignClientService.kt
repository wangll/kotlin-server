package com.example.demo.service

import com.example.demo.config.OpenFeignConfig
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestParam

@FeignClient(name="FeignClientService" ,configuration =[OpenFeignConfig::class] ,url = "https://git-biz.qianxin-inc.cn", path = "/api/v4/projects")
interface FeignClientService {
    @GetMapping("/{projectId}/members/all")
    fun getGitUserData(@PathVariable projectId: String, @RequestParam("private_token") gitToken: String): Any

}