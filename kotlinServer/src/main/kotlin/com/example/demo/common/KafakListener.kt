package com.example.demo.common

import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.transaction.annotation.Transactional

@Transactional("kafkaTransactionManager")
class KafakListener {


}
@KafkaListener(topics = ["UpdateProjectTopic","UpdateWorkingTimeTopic"])
fun onMessagel(record:ConsumerRecord<String, Array<Byte>>) {
    println("简单消费："+record.topic()+"-"+record.partition()+"-"+record.value())
}