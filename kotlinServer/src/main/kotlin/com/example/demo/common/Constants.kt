package com.example.demo.common

class Constants {
    companion object {
        const val MSG_NO_MAIL = "用户无邮箱地址"
        const val MSG_NO_ID = "用户无工号"
        const val MSG_NO_AUTH = "无操作权限"
        const val DATE_FORMAT_ERROR = "日期格式错误"
        const val MSG_WRONG_MAIL = "用户邮箱不准确"
        const val MSG_DUP_REQ = "提交频率过高"
        const val MSG_NO_USER = "找不到用户信息"
        const val MSG_NO_DEPARTMENT = "找不到部门信息"


        const val yyyy_MM_dd = "yyyy-MM-dd"
        const val yyyyMMdd = "yyyyMMdd"
        const val yyyy = "yyyy"
        const val yyyy_MM = "yyyy-MM"
        const val HH_mm_ss = "HH:mm:ss"
        const val yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss"
        const val yyyy_MM_dd_HH_mm = "yyyy-MM-dd HH:mm"
        const val WORK_LOG_MIN_PERIOD = 15 // 工作日志的时间范围最小为15分钟

        const val VIRTUAL_WORK_LOG_PROJECT_ID = "00000000-0000-0000-0000-000000000000"
        const val VIRTUAL_WORK_LOG_PROJECT_NAME = "无归属项目"
        const val UNKNOWN_PROJECT = "未知项目"

        const val workingTimeNotifySetting = "sysSet/workingTime_notify"
        const val staffBasicDataSearch = "sysSet/personnel"
        const val importAuthority = "import"

        const val SYSTEM_START_DATE = "2019-04-01"

        const val ROOT_ROLE = "默认超级用户"
        const val PRODUCT_MANAGER = "产品运营管理部"

        const val Period_Pending = "考核周期未在进行中"
        const val NOT_IN_SELF_REVIEW = "当前考核参与人不在自评状态中"
        const val UNIT_NOT_IN_ONGOING = "所在考核单元未开始或已提交！"
        const val NOT_IN_MAKING_INDICATOR = "不在制定指标周期内"
        const val NOT_PERFORMANCE_ADMIN = "非绩效管理员，无此操作权限"
        const val NOT_PERFORMANCE_ADMIN_OR_HRBP = "非绩效管理员或HRBP，无此操作权限"
        const val PERIOD_NOT_EXISTED = "当前考核周期不存在"
        const val UNIT_NOT_EXISTED = "当前考核单元不存在"
        const val PERIOD_ALREADY_FINISH = "考核周期已结束，不可修改"
        const val PARTICIPANT_NOT_EXISTED = "考核参与人不存在"
        const val PERSON_MONTH_TO_DAY = 21.75F
        const val BUDGET_TEN_THOUSAND = 10000
        const val TR_REVIEW = "TR评审"
        const val RDBPROLE = "R&D-BP"
        const val PMROLE = "项目经理"
        const val SIX_DIGITS  = "%06d"
        const val FOUR_DIGITS  = "%04d"

        const val PROJECT_WRAN_EMAIL_SUBJECT = "项目指标提醒:  "
        val VERIFY_WORKLOG_PROJECT_STAUTS =  setOf("已关闭","项目结项","已结项","项目终止")




    }
}
