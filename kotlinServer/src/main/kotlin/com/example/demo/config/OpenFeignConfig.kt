package com.example.demo.config

import feign.Logger
import feign.codec.Encoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.boot.autoconfigure.http.HttpMessageConverters

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.openfeign.support.SpringEncoder
import feign.form.spring.SpringFormEncoder


@Configuration
class OpenFeignConfig {
    @Autowired
    private val messageConverters: ObjectFactory<HttpMessageConverters>? = null

    @Bean
    fun feignFormEncoder(): Encoder? {
        return SpringFormEncoder(SpringEncoder(messageConverters))
    }

    @Bean
    fun feignLoggerLevel(): Logger.Level? {
        return Logger.Level.FULL
    }
}