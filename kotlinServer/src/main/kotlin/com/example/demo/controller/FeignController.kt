package com.example.demo.controller

import com.example.demo.service.FeignClientService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.GetMapping




@RequestMapping("/api/v4/projects")
@RestController
class FeignController {

    @Autowired
    lateinit var feignClient: FeignClientService

    @RequestMapping("/{projectId}/members/all")
    fun test(
        @PathVariable projectId: String,
        @RequestParam private_token: String
    ): Any {
        println(private_token)
       return feignClient.getGitUserData(projectId, private_token)
    }


}