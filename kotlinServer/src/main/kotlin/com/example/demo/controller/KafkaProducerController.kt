package com.example.demo.controller

import com.example.demo.common.KafkaProducer
import com.example.demo.common.DateUtils
import net.es360.tengyun.commons.kafkamsg.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class KafkaProducerController {

    @Autowired
    private lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @Autowired
    lateinit var producer: KafkaProducer

    // 发送消息
    @GetMapping("/kafka/normal/{message}")
    fun sendMessage1(@PathVariable("message") normalMessage: String) {
        println(normalMessage)
        val model = UpdateProjectMessagePayLoad(normalMessage, WorkingTimeOperation.ProjectDelete)

        val normalMessage = KafkaMsgData(UUID.randomUUID().toString(),
            KafkaMsgType.UpdateWorkingTimeType,
            DateUtils.format(Date(), "yyyy-MM-dd HH:mm:ss"),
            "userId",
            model)

        producer.send(KafkaTopic.UpdateWorkingTimeTopic, null, normalMessage)
    }

    /*
     * @Description 发送项目消息
     * @Date 2021/8/13 18:18
     */
    fun sendProjectMessage(projectId: String, workingTimeOperation: WorkingTimeOperation, userId: String) {
        val model = UpdateProjectMessagePayLoad(projectId, WorkingTimeOperation.ProjectDelete)
        val message = KafkaMsgData(UUID.randomUUID().toString(),
            KafkaMsgType.UpdateProjectType,
            DateUtils.format(Date(), "yyyy-MM-dd HH:mm:ss"),
            userId,
            model)
        producer.send(KafkaTopic.UpdateWorkingTimeTopic, null, message)
    }


}
