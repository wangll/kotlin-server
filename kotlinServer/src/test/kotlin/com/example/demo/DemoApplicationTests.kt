package com.example.demo

import com.example.demo.common.Constants
import com.example.demo.common.DateUtils
import org.assertj.core.util.DateUtil
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.time.LocalDate
import java.time.LocalDate.now
import java.time.format.DateTimeFormatter
import java.util.*

//@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DemoApplicationTests {

	@Test
	fun contextLoads() {
		println(DateUtils.dateNow())
		println(DateUtil.now())
		println(DateUtils.parseDate(now().toString(),Constants.yyyy_MM_dd))
		val formatter = DateTimeFormatter.ofPattern(Constants.yyyy_MM_dd)

		println(LocalDate.parse(now().toString(), formatter))
	}

}
